import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environment/environment';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add auth header with jwt if account is logged in and request is to the api url
        const getToken = localStorage.getItem('token');
        const isApiUrl = request.url.startsWith(environment.apiUrl);
        if (getToken && isApiUrl) {
            request = request.clone({
                setHeaders: { Authorization: ` ${getToken}` }
            });
        }

        return next.handle(request);
    }
}
