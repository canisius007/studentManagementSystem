import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environment/environment';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {


  hide = true;

  constructor(private builder:FormBuilder, private http: HttpClient, private router: Router, private authService: AuthService){

    if (this.authService.isLoggedIn) {
      this.router.navigate(['/admin']);
    }
  }

  loginform = this.builder.group({
    email:this.builder.control('',[Validators.required,Validators.email]),
    password:this.builder.control('',[Validators.required,Validators.minLength(6)])
  })

  get f(){
    return this.loginform.controls;
  }

 public Proceedlogin(){
    if(this.loginform.valid){

      const url = `${environment.apiUrl}/admin/login`;
      const body ={
        email: this.loginform.value.email,
        password: this.loginform.value.password
      };

      this.http.post<any>(url, body).subscribe((res: any)=>{
        if(res){
          localStorage.setItem('token', res.token);
          alert("Login Successful");
          console.log('login successfully');
          this.loginform.reset();

         this.router.navigate(['/admin']);
         }else{
          console.log('user is not found')
        }
      },(_err:any)=> {
        console.error(_err);
      });    
    }
  }
}





  

