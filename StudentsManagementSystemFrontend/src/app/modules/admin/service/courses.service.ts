import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';


@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private _http: HttpClient) { }

  addCourse(data: any): Observable<any> {
    return this._http.post(`${environment.apiUrl}/course/create`, data);
  }

  getCourse(): Observable<any> {
    return this._http.get(`${environment.apiUrl}/course/list`);
  }

  deleteCourse(_id: number): Observable<any> {
    return this._http.delete(`${environment.apiUrl}/course/delete/${_id}`);
  }

  updateCourse(_id: number, data: any): Observable<any> {
    return this._http.patch(`${environment.apiUrl}/course/update/${_id}`, data);
  }
}
