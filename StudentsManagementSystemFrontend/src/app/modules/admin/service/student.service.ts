import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';


@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private _http: HttpClient) { }

  addStudent(data: any): Observable<any> {
    return this._http.post(`${environment.apiUrl}/student/add`, data);
  }

  updateStudent(_id: number, data: any): Observable<any> {
    return this._http.patch(`${environment.apiUrl}/student/update/${_id}`, data);
  }

  getStudentList(): Observable<any> {
    return this._http.get(`${environment.apiUrl}/student/list`);
  }

  deleteStudent(_id: number): Observable<any> {
    return this._http.delete(`${environment.apiUrl}/student/delete/${_id}`);
  }
}
