import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class BatchService {

  constructor(private _http: HttpClient) { }

  addBatch(data: any): Observable<any>{
    return this._http.post(`${environment.apiUrl}/batch/add`, data);
  }

  getBatch(): Observable<any>{
    return this._http.get(`${environment.apiUrl}/batch/list`);
  }

  deleteBatch(_id: number): Observable<any>{
    return this._http.delete(`${environment.apiUrl}/batch/delete/${_id}`);
  }

  updateBatch(_id: number, data: any): Observable<any>{
    return this._http.patch(`${environment.apiUrl}/batch/update/${_id}`, data);
  }
}
 