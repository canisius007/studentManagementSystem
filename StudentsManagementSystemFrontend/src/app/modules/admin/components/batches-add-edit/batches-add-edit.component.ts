import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BatchService } from '../../service/batch.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CoreService } from 'src/app/core/core.service';

@Component({
  selector: 'app-batches-add-edit',
  templateUrl: './batches-add-edit.component.html',
  styleUrls: ['./batches-add-edit.component.css']
})
export class BatchesAddEditComponent implements OnInit {

  batchForm: FormGroup;

  constructor(private _bb: FormBuilder, 
    private _batchService: BatchService, 
    private _dialogRef: MatDialogRef<BatchesAddEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data :any,
    private _coreService: CoreService
    ) { 
    this.batchForm = this._bb.group({
      batch: '',
    });
  }

  ngOnInit(): void {
    this.batchForm.patchValue(this.data)
  }

  onBatchSubmit(){
    if(this.batchForm.valid){
      if(this.data) {
        this._batchService.updateBatch(this.data._id, this.batchForm.value).subscribe({
          next: (val: any) => {
            // alert('batch detail Updated');
            this._coreService.openSnackBar('batch detail Updated');
            this._dialogRef.close(true);
          },
          error: (err: any) => {
            console.log(err);
          },
        });
      }else{
        this._batchService.addBatch(this.batchForm.value).subscribe({
          next: (val: any) => {
            // alert('batch is successfully');
            this._coreService.openSnackBar('course is successfully');
            this._dialogRef.close(true);
          },
          error: (err: any) => {
            console.log(err);
            this._coreService.openSnackBar('This type Course is already exists', 'cancel');
          },
        });
      }
      
    }
  }
}
