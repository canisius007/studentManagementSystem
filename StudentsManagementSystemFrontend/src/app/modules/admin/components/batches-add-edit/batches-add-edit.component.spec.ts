import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchesAddEditComponent } from './batches-add-edit.component';

describe('BatchesAddEditComponent', () => {
  let component: BatchesAddEditComponent;
  let fixture: ComponentFixture<BatchesAddEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BatchesAddEditComponent]
    });
    fixture = TestBed.createComponent(BatchesAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
