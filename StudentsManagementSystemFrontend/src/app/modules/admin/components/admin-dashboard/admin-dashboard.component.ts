import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout'

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent {
  sideBarOpen = false;

  constructor(private breakpointObserver: BreakpointObserver) { }

  ngOnInit() {
    this.breakpointObserver.observe([Breakpoints.Handset])
      .subscribe(result => {
        if (result.matches) {
          // For handset screens (small screens), close the sidebar
          this.sideBarOpen = false;
        } else {
          // For larger screens, open the sidebar
          this.sideBarOpen = true;
        }
      });
  }


  sidebartoggle(){
    this.sideBarOpen=!this.sideBarOpen;
  }
}
