import { Component, OnInit} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EmptyAddEditComponent } from '../empty-add-edit/empty-add-edit.component';
import { StudentService } from '../../service/student.service';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  displayedColumns: string[] = ['_id', 'fullName', 'email', 'phoneNumber', 'dob', 'address', 'batch', 'course', 'edit', 'delete'];
  dataSource = new MatTableDataSource<any>;


  constructor(private _dialog: MatDialog, private _studService: StudentService) { }

  ngOnInit(): void {
    this.getStudentList();
  }

  getFullName(element: any): string {
    return `${element.firstName} ${element.lastName}`;
  }

  openAddEditEmptyForm() {
   const DialogRef = this._dialog.open(EmptyAddEditComponent);
   DialogRef.afterClosed().subscribe({
    next: (val) => {
      if (val) {
        this.getStudentList();
      }
    }
   });
  }

  getStudentList() {
    this._studService.getStudentList().subscribe({
      next: (res) => {
        this.dataSource = new MatTableDataSource(res);
      },
      error: console.log,
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }

  deleteStudent(_id: number){
    this._studService.deleteStudent(_id).subscribe({
      next: (res)=>{
        alert('student deleted');
        this.getStudentList();
      },
      error:console.log,
    })
  }

  updateStudent(data: any) {
   const DialogRef = this._dialog.open(EmptyAddEditComponent,{
    data,
   });

   DialogRef.afterClosed().subscribe({
    next: (val) => {
      if (val) {
        this.getStudentList();
      }
    }
   });
  
   
   }
}
