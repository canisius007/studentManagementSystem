import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl:'./header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  @Output() toggleSidebarForMe: EventEmitter<any> = new EventEmitter();
  menu: any;
  constructor(private router: Router){}

  toggleSidebar(){
    this.toggleSidebarForMe.emit();
  }

  logout(){
    const confirmation = confirm('Do you want to logout');
    if(confirmation){
      localStorage.removeItem('token');
      this.router.navigate(['/login']);    
    }
  }
}
