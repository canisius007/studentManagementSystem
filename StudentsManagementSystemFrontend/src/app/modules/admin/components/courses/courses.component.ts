import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CoursesService } from '../../service/courses.service';
import { CoursesAddEditComponent } from '../courses-add-edit/courses-add-edit.component';
import { MatTableDataSource } from '@angular/material/table';
import { CoreService } from 'src/app/core/core.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent {

  displayedColumns: string[] = ['_id', 'course','createdAt', 'updatedAt', 'action'];
  dataSource = new MatTableDataSource<any>;


  constructor (private _dialog: MatDialog,
     private _courseService: CoursesService,
     private _coreService: CoreService){}

  ngOnInit(): void {
    this.getCourse();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openAddEditCourse(){
    const DialogRef = this._dialog.open(CoursesAddEditComponent);
    DialogRef.afterClosed().subscribe({
      next: (val) => {
        this._coreService.openSnackBar('Course Added Successfully', 'done');
        if(val){
          this.getCourse();
        }
      }
    });
  }

  getCourse(){
    this._courseService.getCourse().subscribe({
      next: (res) => {
        console.log(res);
        this.dataSource = new MatTableDataSource(res);
      },
      error: console.log
    })
  }

  deleteCourse(_id: number){    
    this._courseService.deleteCourse(_id).subscribe({
      next: (res) => {
        // alert('Deleted Successfully');
        this._coreService.openSnackBar('deleted successfully','done');
        this.getCourse();
      },
      error: console.log
    })
  }

  updateCourse(data: any){
    const DialogRef = this._dialog.open(CoursesAddEditComponent, {
      data,
     });
  
     DialogRef.afterClosed().subscribe({
      next: (val) => {
        if(val){
          this.getCourse();
        }
      }
    });
      
    }
}
