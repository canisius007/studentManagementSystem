import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BatchesAddEditComponent } from '../batches-add-edit/batches-add-edit.component';
import { BatchService } from '../../service/batch.service';
import {MatTableDataSource} from '@angular/material/table';
import { CoreService } from 'src/app/core/core.service';


@Component({
  selector: 'app-batches',
  templateUrl: './batches.component.html',
  styleUrls: ['./batches.component.css']
})
export class BatchesComponent implements OnInit {

  displayedColumns: string[] = ['_id', 'batch','createdAt', 'updatedAt', 'action'];
  dataSource = new MatTableDataSource<any>;

  constructor (private _dialog: MatDialog, 
    private _batchService: BatchService,
    private _coreService: CoreService){}

  ngOnInit(): void {
    this.getBatch();
  }

  
  openAddEditBatch(){
    const DialogRef = this._dialog.open(BatchesAddEditComponent);
    DialogRef.afterClosed().subscribe({
      next: (val) => {
        this._coreService.openSnackBar('Batches Added Successfully', 'done');
        if(val){
          this.getBatch();
        }
      }
    });
  }

  getBatch(){
    this._batchService.getBatch().subscribe({
      next: (res) => {
        console.log(res);
        this.dataSource = new MatTableDataSource(res);
      },
      error: console.log
    })
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  deleteBatch(_id: number){    
    this._batchService.deleteBatch(_id).subscribe({
      next: (res) => {
        // alert('Deleted Successfully');
        this._coreService.openSnackBar('deleted successfully','done');
        this.getBatch();
      },
      error: console.log
    })
  }

  updateBatch(data: any){
  const DialogRef = this._dialog.open(BatchesAddEditComponent, {
    data,
   });

   DialogRef.afterClosed().subscribe({
    next: (val) => {
      if(val){
        this.getBatch();
      }
    }
  });
    
  }

}
