import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StudentService } from '../../service/student.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CoursesService } from '../../service/courses.service';
import { BatchService } from '../../service/batch.service';
import { CoreService } from 'src/app/core/core.service';

@Component({
  selector: 'app-empty-add-edit',
  templateUrl: './empty-add-edit.component.html',
  styleUrls: ['./empty-add-edit.component.css']
})
export class EmptyAddEditComponent implements OnInit {

  Form: FormGroup;
  
  _id: string ='';
  Course: any[] = [];
  Batch: any[] = [];

  constructor(private _fb: FormBuilder, 
    private _studService: StudentService, 
    private _courseService:CoursesService,
    private _batchService:BatchService,
    private _dialogRef: MatDialogRef<EmptyAddEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _coreService: CoreService){

      this.Form = this._fb.group({
        firstName: ['', [Validators.required]],
        lastName: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        phoneNumber: ['', [Validators.required]],
        dob: ['', [Validators.required]],
        address: ['', [Validators.required]],
        batch: ['', [Validators.required]],
        course: ['', [Validators.required]],
      });

}



ngOnInit(): void {
  // console.log(this.data)
  if(this.data){
    this.Form.patchValue(this.data);
    this.Form.patchValue(
      {batch: this.data.batch._id,
      course: this.data.course._id}
      );
  }   
  // this.Form.patchValue();  
    // console.log(this.data);
  this.loadCourse();
  this.loadBatch();
}



loadCourse(){
  this._courseService.getCourse().subscribe({
    next: (data: any) => {
      this.Course = data;
    }
  })
}

loadBatch(){
  this._batchService.getBatch().subscribe({
    next: (data: any) => {
      this.Batch = data;
    }
  })
}


onFormSubmit(){
  if(this.Form.valid){
    if(this.data){
      this._studService.updateStudent(this.data._id, this.Form.value).subscribe({
        next:(val: any) =>{
          // alert('student detail updated');
          this._coreService.openSnackBar('studentlist detail Updated');
          this._dialogRef.close(true);
        },
        error: (err: any)=>{
          console.log(err)
        }
      });
    }else {
      
      this._studService.addStudent(this.Form.value).subscribe({
        next:(val: any) =>{
          alert('student added successfully');
          this._coreService.openSnackBar('StudentList is successfully');
          this._dialogRef.close(true);
        },
        error: (err: any)=>{
          this._coreService.openSnackBar('This type StudentList is already exists', 'cancel');
          console.log(err)
        }
      });
    }
  }
}
}
