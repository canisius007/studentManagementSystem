import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptyAddEditComponent } from './empty-add-edit.component';

describe('EmptyAddEditComponent', () => {
  let component: EmptyAddEditComponent;
  let fixture: ComponentFixture<EmptyAddEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EmptyAddEditComponent]
    });
    fixture = TestBed.createComponent(EmptyAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
