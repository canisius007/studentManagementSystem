import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesAddEditComponent } from './courses-add-edit.component';

describe('CoursesAddEditComponent', () => {
  let component: CoursesAddEditComponent;
  let fixture: ComponentFixture<CoursesAddEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CoursesAddEditComponent]
    });
    fixture = TestBed.createComponent(CoursesAddEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
