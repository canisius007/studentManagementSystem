import { Component, Inject, OnInit } from '@angular/core';
import {FormBuilder, FormGroup } from '@angular/forms';
import { CoursesService } from '../../service/courses.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CoreService } from 'src/app/core/core.service';

@Component({
  selector: 'app-courses-add-edit',
  templateUrl: './courses-add-edit.component.html',
  styleUrls: ['./courses-add-edit.component.css']
})
export class CoursesAddEditComponent implements OnInit {

  courseForm: FormGroup;

  constructor(private _bb: FormBuilder, 
    private _courseService: CoursesService, 
    private _dialogRef: MatDialogRef<CoursesAddEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data :any,
    private _coreService: CoreService
    ) { 
    this.courseForm = this._bb.group({
      course: '',
    });
  }

  ngOnInit(): void {
    this.courseForm.patchValue(this.data)
  }


  onCourseSubmit(){
    if(this.courseForm.valid){
      if(this.data) {
        this._courseService.updateCourse(this.data._id, this.courseForm.value).subscribe({
          next: (val: any) => {
            // alert('course detail Updated');
            this._coreService.openSnackBar('course detail Updated');
            this._dialogRef.close(true);
          },
          error: (err: any) => {
            console.log(err);
          },
        });
      }else{
        this._courseService.addCourse(this.courseForm.value).subscribe({
          next: (val: any) => {
            // alert('course is successfully');
            this._coreService.openSnackBar('course is successfully');
            this._dialogRef.close(true);
          },
          error: (err: any) => {
            this._coreService.openSnackBar('This type Course is already exists', 'cancel');
          },
        });
      }
      
    }
  }
}
