import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { StudentListComponent } from './components/student-list/student-list.component';
import { BatchesComponent } from './components/batches/batches.component';
import { CoursesComponent } from './components/courses/courses.component';
import { SettingComponent } from './components/setting/setting.component';

const routes: Routes = [
  {path: '', component: AdminDashboardComponent, children:[
    {path: 'dashboard', component: DashboardComponent},
    {path: 'student-list', component: StudentListComponent},
    {path: 'batches', component: BatchesComponent},
    {path: 'courses', component: CoursesComponent},
    {path: 'setting', component: SettingComponent},
    {path: '', redirectTo: '/admin/dashboard', pathMatch: 'full'},
  ],
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
