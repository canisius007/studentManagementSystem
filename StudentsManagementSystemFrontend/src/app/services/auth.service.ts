import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  public IsLoggedIn(){
    return !! localStorage.getItem('token',);
  }

  get isLoggedIn(): boolean {
    return !!localStorage.getItem('token');
  }
}
