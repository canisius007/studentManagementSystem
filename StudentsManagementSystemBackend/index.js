import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config();
import adminRoute from "./Route/adminRoute.js";
import studentRoute from "./Route/studentsRoute.js";
import batchRoute from "./Route/batchRoute.js";
import courseRoute from "./Route/courseRoute.js";
import statusRoute from "./Route/statusRoute.js";

import cors from 'cors'

const app = express();

app.use(bodyParser.json());
app.use(cors())
mongoose.connect(process.env.URI)
    .then(() => console.log('Connected to DataBase'))
    .catch((err) => console.log(err));

app.use('/admin', adminRoute)
app.use('/student', studentRoute)
app.use('/batch', batchRoute)
app.use('/course', courseRoute)
app.use('/status',statusRoute)


app.listen(process.env.PORT, () => {
    console.log(`Server is running at ${process.env.PORT}`);
})