import Status from '../Model/statusSchema.js'

// Creating a status

async function createStatus(req,res){
    try {
        const newStatus =new Status(req.body)
        const savedStatus = await newStatus.save()
        res.status(200).json(savedStatus)

    } catch (error) {
        console.error(error);
        res.status(400).json ({error:'Internal Credential'})
    }
}

// list the status

async function listStatus(req,res){
    try {
        const listCourse = await Status.find()
        res.json(listCourse)
    } catch (error) {
        console.error(error);
        res.status(500).json({error:'Internal Credential'})
    }
}

// update the status

async function updateStatus(req,res){
    try {
        const statusId = req.params.id;
        const updatedData = req.body;
        
        const updatedStatus = await Status.findByIdAndUpdate(statusId,{$set: updatedData}, {new: true});
        res.status(200).json({updatedStatus})
    } catch (error) {
        console.error(error);
        res.status(400).json({error: 'Internal Credential'})
    }
}

// Delete the status

async function deleteStatus(req,res){
   try {
     const statusId = req.params.id;
    const deletedStatus = await Status.findByIdAndDelete(statusId)

     if (deletedStatus) {
        res.json({ message: 'Course deleted successfully' });
      } else {
        res.status(404).json({ error: 'Course not found' });
      }
}
   catch (error) {
     console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
   }
}

export {createStatus,updateStatus, listStatus, deleteStatus}