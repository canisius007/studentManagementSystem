//admin route function

import { Admin } from "../Model/adminSchema.js";
// import { Students } from "../Model/studentsSchema.js";
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';

//  middleware for registration admin
async function adminRegister(req, res) { 
    console.log(req.body);                         
    try {                                        
        const newAdmin = new Admin(req.body);
        newAdmin.password = await bcrypt.hash(String(newAdmin.password), 10)
        const savedAdmin = await newAdmin.save();
        res.status(200).json(savedAdmin);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
}

// middleware for dashboard to get all student data
async function adminDashboard(req, res) {
    try {
        const admin = await Admin.find();
        res.json(admin);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
    }
}

//middleware for login
async function adminLogin(req, res) {
    try {
        const { email, password } = req.body;

        if (!email || !password) {
            return res.status(400).json({ error: 'Email and Password must be provided' });
        }

        const admin = await Admin.findOne({ email });

        if (!admin) {
            return res.status(400).json({ error: 'User does not exist' });
        }

        const isPasswordValid = await admin.verifyPassword(password);

        if (!isPasswordValid) {
            return res.status(400).json({ error: 'Incorrect Password' });
        }

        const payload = {
            email: admin.email,
        };

        const token = jwt.sign(payload, process.env.SECRETKEY, { expiresIn: '1h' });

        // data: admin.email

        res.json({ token, });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
    }
}

//to verify token


export {adminDashboard, adminRegister, adminLogin}
