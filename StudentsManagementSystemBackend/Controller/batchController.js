import Batch from "../Model/batchSchema.js";

// Middleware for creating a batch
async function createBatch(req, res) {
    try {
        const newBatch = new Batch(req.body);
        const savedBatch = await newBatch.save();
        res.status(200).json(savedBatch);
    } catch (error) {
        console.log(`The error in batch creation\n\n${error.name}`);
        if (error.name === 'ValidationError' && error.errors.batch) {
            res.status(400).json({ error: error.errors.batch.message });
        } else {
            console.error(error);
            res.status(500).json({ error: 'Internal Credential' });
        }
    }       
}

// Middleware to read/get batch data
async function readBatch(req, res) {
    try {
        const batches = await Batch.find();
        res.json(batches);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Credential' });
    }
}

// Middleware for updating batch data
async function patchBatch(req, res) {
    try {
        const batchId = req.params.id;
        const updateData = req.body;

        // Validate if updateData is not empty
        if (!updateData || Object.keys(updateData).length === 0) {
            return res.status(400).json({ error: 'No data provided for update' });
        }

        // Find and update batch
        const updatedBatch = await Batch.findByIdAndUpdate(batchId, updateData, { new: true });

        if (!updatedBatch) {
            return res.status(404).json({ error: 'Batch not found' });
        }

        res.status(200).json(updatedBatch);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
    }
}

// Middleware for deleting batch
async function deleteBatch(req, res) {
    try {
        const batchId = req.params.id;
        // Find and delete batch
        const deletedBatch = await Batch.findByIdAndDelete(batchId);

        if (!deletedBatch) {
            return res.status(404).json({ error: 'Batch not found' });
        }

        res.status(200).json({ message: 'Batch deleted successfully' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
    }
}

export { createBatch, readBatch, patchBatch, deleteBatch };