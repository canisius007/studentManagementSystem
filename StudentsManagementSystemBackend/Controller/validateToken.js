import jwt from 'jsonwebtoken';

async function validateToken(req, res, next) {
    try {
        const token = req.header('Authorization');
        const verifyToken = jwt.verify(token, process.env.SECRETKEY)
        if (verifyToken) {
            console.log("Access Granted");
        } else {
            console.log("Access Denied");
        }
        next();
    } catch (error) {
        if(error.name === "JsonWebTokenError"){
            res.status(401).json({ error: "Unautorized!" })
        } else {
            console.error(error);
            res.status(500).json({ error: "Internal Credential"});
        }
       
    }
}

export default validateToken