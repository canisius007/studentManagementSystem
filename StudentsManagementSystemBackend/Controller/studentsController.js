// student controller function
import  Students from '../Model/studentsSchema.js'
// import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';


// middleware for students registration
async function addStudent(req, res) {                         
    try {                                       
        // Create a new student object from the request body
        const newStudent = new Students(req.body);
        const savedStudent = await newStudent.save();

        // await savedStudent.populate('batch', 'batch').populate('course', 'course').execPopulate();
        
        res.status(200).json(savedStudent);
    } catch (err) {
        res.status(500).json({ message:err.message });
    }
}

// middleware to get students details
// async function listStudent(req, res) {
//     try {
//         const students = await Students.find()
//             .populate('batch', 'batch') 
//             .populate('course', 'course');

//         res.json(students);
//     } catch (error) {
//         console.error(error);
//         res.status(500).json({ error: 'Internal Credential' });
//     }
// }


async function listStudent(req, res) {
    try {
        const page = parseInt(req.query.page) || 1;
        const pageSize = parseInt(req.query.pageSize) || 10;

        const totalCount = await Students.countDocuments();
        const totalPages = Math.ceil(totalCount / pageSize);

        const students = await Students.find()
            .populate('batch', 'batch') 
            .populate('course', 'course')
            .skip((page - 1) * pageSize)
            .limit(pageSize);

        res.json({
            students,
            currentPage: page,
            pageSize,
            totalPages,
            totalCount,
        });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
}


//middleware to patch students details
async function updateStudent(req, res) {
    try {
        const studentId = req.params.id;
        const updateData = req.body;

        const updateStudent = await Students.findByIdAndUpdate(studentId, {$set: updateData}, {new: true})
        // .populate('batch', 'batch') 
        // .populate('course', 'course'); 

        if (!updateStudent) {
            return res.status(404).json({error: 'Batch not found'})
        }
        res.status(200).json(updateStudent)
    } catch (error) {
        console.error(error);
        res.status(500).json({error: 'Internal server error'})
    }
}

//middleware to delete student data
async function deleteStudent(req, res) {
    try {
        const studentId = req.params.id;

        const deleteStudent = await Students.findByIdAndDelete(studentId)
        // .populate('batch', 'batch') 
        //     .populate('course', 'course'); 

        if(!deleteStudent) {
            return res.status(404).json({error: 'Student not found'})
        }
        res.status(200).json({ message: 'Student deleted Successfully'})
    } catch (error) {
        console.error(error);
        res.status(500).json({error: 'Internal server error'})
    }
}

export { addStudent, listStudent, updateStudent,deleteStudent}

