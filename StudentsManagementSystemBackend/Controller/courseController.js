
// CourseController.js
import Course from '../Model/courseSchema.js'

// Creating the Course 
async function createCourse(req, res){
  try {
      
   const newCourse = new Course(req.body)
   const savedCourse = await newCourse.save()
   res.status(200).json(savedCourse)
  
  }catch (error) {
    if (error.name === 'ValidationError' && error.errors.course) {
        res.status(400).json({ error: error.errors.course.message });
    } else {
        console.error(error);
        res.status(400).json({ error: 'Internal Credential' });
    }
    
}
}
// listing the Course

async function listCourse(req, res){
    try {
        const listCourse = await Course.find()
        res.json(listCourse)

    } catch (error) {
        console.error(error);
        res.status(500).json({error: "Internal Credential"})
    }
}

// update the Course

//middleware for PATCH for course data
async function updateCourse(req, res) {
    try {
        const courseId = req.params.id;
        const updateData = req.body;

        // Validate if updateData is not empty
        if (!updateData || Object.keys(updateData).length === 0) {
            return res.status(400).json({ error: 'No data provided for update' });
        }

        // Find and update course
        const updatedCourse = await Course.findByIdAndUpdate(courseId, { $set: updateData }, { new: true });

        if (!updatedCourse) {
            return res.status(404).json({ error: 'Course not found' });
        }

        res.status(200).json(updatedCourse);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
    }
}
// delete the Course
async function deleteCourse(req, res) {
    try {
      const courseId = req.params.id;
  
      const deletedCourse = await Course.findByIdAndDelete(courseId);
  
      if (deletedCourse) {
        res.json({ message: 'Course deleted successfully' });
      } else {
        res.status(404).json({ error: 'Course not found' });
      }
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }


export {createCourse, listCourse, updateCourse, deleteCourse}