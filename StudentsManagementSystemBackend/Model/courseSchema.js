import mongoose from "mongoose";

const courseSchema = new mongoose.Schema({
    course: {
        type: String,
        required: true,
        unique: true,
        validate: {
            // Custom validation
            validator: async function(value) {
                const existingCourse = await this.constructor.findOne({ course: value });
                return !existingCourse;
            },
            message: 'This type Course is already exists'
        }
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => Date.now(),
    },
    updatedAt: {
        type: Date,
        default: () => Date.now(),
    }
});

    
const Course = mongoose.model("Course", courseSchema);

export default Course;