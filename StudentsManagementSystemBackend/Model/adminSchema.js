import mongoose from "mongoose";
import bcrypt from "bcryptjs"

const adminSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

adminSchema.methods.verifyPassword = async function(candidatePassword) {
    console.log('Entered Password:', candidatePassword);
    console.log('Stored Password:', this.password);

    try {
        return await bcrypt.compare(String(candidatePassword), this.password)
    } catch (error) {
        console.error(error);
        throw new Error('Password verification failed')
    }
};

const Admin = mongoose.model("Admin", adminSchema)

export { Admin }

//schema