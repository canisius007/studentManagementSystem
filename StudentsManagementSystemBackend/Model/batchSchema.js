import mongoose from "mongoose";

const batchSchema = new mongoose.Schema({
    batch: {
        type: String,
        required: true,
        unique: true,  
        validate: {
            // Custom validation
            validator: async function(value) {
                const existingBatch = await this.constructor.findOne({ batch: value });
                return !existingBatch;
            },
            message: 'This batch already exists'
        }        
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => Date.now(),
    },
    updatedAt: {
        type: Date,
        default: () => Date.now(),
    }
});

const Batch = mongoose.model("Batch", batchSchema);

export default Batch;
