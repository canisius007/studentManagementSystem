import mongoose from "mongoose";

const statusSchema = new mongoose.Schema({
    status: {
        type: String,
        required: true,
        unique: true,
        validate: {
            // Custom validation
            validator: async function(value) {
                const existingStatus = await this.constructor.findOne({ status: value });
                return !existingStatus;
            },
            message: 'This status is already exists'
        }
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => Date.now(),
    },
    updatedAt: {
        type: Date,
        default: () => Date.now(),
    }
});

    
const Status = mongoose.model("Status", statusSchema);

export default Status;