import mongoose from "mongoose";
// import Batch from "./batchSchema.js";
// import Course from "./courseSchema.js";

const studentSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    phoneNumber: {
        type: Number,
    },
    dob: {
        type: Date,
        required: true,
    },
    address: {
        type: String,
    },
    // status: {
    //     type:mongoose.Schema.Types.ObjectId,
            // ref:"Status"   
    // },
    batch: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Batch",
    },
    course: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Course",
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => Date.now(),
    },
    updatedAt: {
        type: Date,
        default: () => Date.now(),
    },
});

const Students = mongoose.model("Students", studentSchema);

export default Students ;
