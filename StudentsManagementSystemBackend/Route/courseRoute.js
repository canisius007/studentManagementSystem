import express from 'express';

import {createCourse, listCourse, updateCourse,deleteCourse} from '../Controller/courseController.js'

import validateToken from '../Controller/validateToken.js';
const courseRoute = express()



courseRoute.post('/create',validateToken,createCourse)

courseRoute.get('/list',validateToken,listCourse)

courseRoute.patch('/update/:id',validateToken,updateCourse)

courseRoute.delete('/delete/:id',validateToken,deleteCourse)


export default courseRoute