import express from "express";
import {adminDashboard, adminRegister, adminLogin} from "../Controller/adminController.js"
import validateToken from "../Controller/validateToken.js";
const adminRoute = express();

adminRoute.post('/login',adminLogin ,validateToken);
adminRoute.post('/register', adminRegister);
adminRoute.get('/dashboard', adminDashboard)

export default adminRoute