//students route

import express from "express";
import { addStudent, listStudent, updateStudent,deleteStudent} from "../Controller/studentsController.js";
import validateToken from "../Controller/validateToken.js";

const studentRoute = express();

// studentRoute.post('/register', studentRegister)
studentRoute.get('/list',validateToken, listStudent)

studentRoute.post('/add',validateToken, addStudent)

studentRoute.patch('/update/:id',validateToken, updateStudent)

studentRoute.delete('/delete/:id',validateToken, deleteStudent)

export default studentRoute 