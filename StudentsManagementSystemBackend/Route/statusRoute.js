import  express  from "express";

import {createStatus,updateStatus, listStatus, deleteStatus} from '../Controller/statusController.js';

import validateToken from "../Controller/validateToken.js";
const statusRoute = express()

statusRoute.post('/create',validateToken, createStatus)
statusRoute.get('/list',validateToken, listStatus)
statusRoute.patch('/update/:id',validateToken, updateStatus)
statusRoute.delete('/delete/:id',validateToken, deleteStatus)


export default statusRoute