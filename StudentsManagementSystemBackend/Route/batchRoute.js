import express from "express";
import { createBatch ,readBatch, patchBatch, deleteBatch } from '../Controller/batchController.js'
import validateToken from "../Controller/validateToken.js";
const batchRoute = express();

batchRoute.post('/add', createBatch);
batchRoute.get('/list',validateToken,readBatch);
batchRoute.patch('/update/:id',validateToken, patchBatch);
batchRoute.delete('/delete/:id',validateToken, deleteBatch);

export default batchRoute
